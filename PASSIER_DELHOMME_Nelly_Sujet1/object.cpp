#include "object.h"

#include <iostream>
#include "renderer.h"
#include "glm/gtx/transform.hpp"

#include <exception>




Object::Object(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::string texturePath):m_vb(0), m_uvsb(0), m_texture(0), position(0,0,0), rotationAngles(0,0,0)
{

     m_vb = new VertexBuffer(vertices);
     m_uvsb = new UVBuffer(uvs);

     m_texture = new Texture(texturePath);

}


Object::~Object()
{
    delete m_vb;
    if (m_uvsb) delete m_uvsb;
    if (m_texture) delete m_texture;
}

void Object::Bind() const
{
    m_vb->Bind(0);
    if (m_uvsb) m_uvsb->Bind(1);
    if (m_texture) m_texture->Bind(0);
}

void Object::Unbind() const
{
    m_vb->Unbind();
    if (m_uvsb) m_uvsb->Unbind();
    if (m_texture) m_texture->Unbind();
}



void Object::Draw() const
{
    GLCall(glDrawArrays(GL_TRIANGLES,0, m_vb->getSize()));
}

glm::mat4 Object::getModelMatrix()
{
    glm::mat4 m = glm::rotate(glm::translate(glm::mat4(1), position), rotationAngles.x, glm::vec3(1,0,0));
    m=glm::rotate(m, rotationAngles.y, glm::vec3(0,1,0));
    m=glm::rotate(m, rotationAngles.z, glm::vec3(0,0,1));
    return m;
}

//Fonction qui crée une liste de hauteurs aléatoires puis qui les lisse.
std::vector<std::vector<float>> hauteurs_aleatoires(int taille)
{
    //Hauteurs aléatoires.
    std::vector<std::vector<float>> hauteurs;
    for(int l=0; l<taille-1; l++){
        std::vector<float> ligne;
        for(int c=0; c<taille-1; c++){
            ligne.push_back((rand()%11)/10.0 *10);
        }
        hauteurs.push_back(ligne);
    }

    for(int c=2; c<taille-3; c++){
         for(int l=2; l<taille-3; l++){
             //moyenne
             float moy = 0;
             for(int i=-2; i<3; i++){
                 for(int j=-2; j<3; j++){
                     moy += hauteurs.at(l+i).at(c+j);
                 }
             }
             moy /= 25;
             hauteurs.at(l).at(c) = moy;}
      }
    //Enlever les éléments du bord
    std::vector<std::vector<float>> res;
    for(int l=2; l<taille-3; l++){
        std::vector<float> ligne;
        for(int c=2; c<taille-3; c++){
            ligne.push_back(hauteurs.at(l).at(c));
        }
        res.push_back(ligne);
    }
    return res;
}

std::vector<glm::vec3> vb_mnt(std::vector<std::vector<float>> hauteurs)
{
    //création des triangles
    std::vector<glm::vec3> res;
    for(int c=0; c<hauteurs.size(); c++){
        for(int l=0; l<hauteurs.size(); l++){
            if(c!=hauteurs.size()-1 && l!=hauteurs.size()-1){

                res.push_back({(float) c, hauteurs.at(l).at(c), (float) l});
                res.push_back({(float) c+1, hauteurs.at(l).at(c+1), (float) l});
                res.push_back({(float) c, hauteurs.at(l+1).at(c), (float) l+1});

                res.push_back({(float) c, hauteurs.at(l+1).at(c), (float) l+1});
                res.push_back({(float) c+1, hauteurs.at(l).at(c+1), (float) l});
                res.push_back({(float) c+1, hauteurs.at(l+1).at(c+1), (float) l+1});

            }
        }
    }
    return res;
}

//Fonction qui crée la liste de points nécessaire à la création du uv buffer de l'object.
std::vector<glm::vec2> uvb_mnt(int taille)
{
    std::vector<glm::vec2> res;
    for(int c=0; c<taille; c++){
        for(int l=0; l<taille; l++){
                res.push_back(glm::vec2(1.0f, 1.0f));
                res.push_back(glm::vec2(0.0f, 1.0f));
                res.push_back(glm::vec2(1.0f, 0.0f));

                res.push_back(glm::vec2(0.0f, 0.0f));
                res.push_back(glm::vec2(1.0f, 0.0f));
                res.push_back(glm::vec2(0.0f, 1.0f));

        }
    }
    return res;
}

int Object::deplacementCube(int sens, double deltaTemps, std::vector<std::vector<float>> hauteurs){

    position.x += sens * deltaTemps;

    if(position.x < 1){
        position.x = 1;
    }else if(position.x > hauteurs.size() - 1){
        position.x = hauteurs.size() -1;
    }
    if(position.z < 1){
            position.z = 1;
        }else if(position.z > hauteurs.size() - 1){
            position.z = hauteurs.size() -1;
        }

    if(position.y < hauteurs.at(position.z).at(position.x) +1){
            position.y = hauteurs.at(floor(position.z)).at(floor(position.x)) + 1.2;
    }else if (position.y > hauteurs.at(position.z).at(position.x) +1){
        position.y = hauteurs.at(floor(position.z)).at(floor(position.x)) +1.2;
    }

    //Ne pas tomber des bords du monde
    if (position.x < 1) {
        sens = 1;
    } else if (position.x > hauteurs.size() -2) {
        sens = -1;
    }
    return sens;
}
